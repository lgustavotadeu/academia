package academia

class ContratoController {
    static allowedMethods = [
            save: "POST",
            show: "GET",
            delete: "DELETE",
            update: "PUT",
            index: "GET"
    ]

    ContratoService contratoService

    def save() {
        render contratoService.save(request, response)
    }

    def index() {
        render contratoService.oneDayToEndContract()
    }
}