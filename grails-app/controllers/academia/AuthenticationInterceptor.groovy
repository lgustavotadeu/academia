package academia

import org.springframework.http.HttpStatus


class AuthenticationInterceptor {

    AuthenticationService authenticationService

    AuthenticationInterceptor() {
        match controller: 'pessoa', action: 'update'
    }

    boolean before() {

        Pessoa pessoa = Pessoa.findById(request.getHeader("id"))

        if (pessoa) {
            boolean authorized = authenticationService.authorized(pessoa, request.getHeader("token"))

            if (authorized) {
                return true
            }
        }

        response.status = HttpStatus.UNAUTHORIZED.value()

        return false
        true

    }

    boolean after() { true }

    void afterView() {
        // no-op
    }
}
