package academia

class PessoaController {
    static allowedMethods = [
            save: "POST",
            show: "GET",
            delete: "DELETE",
            update: "PUT",
            index: "GET"
    ]

    PessoaService pessoaService

    def show() {
        render pessoaService.show(params, response)
    }

    def save() {
        render pessoaService.save(request, response)
    }

    def delete() {
        render pessoaService.delete(params, response)
    }

    def update() {
        render pessoaService.update(params, request, response)
    }

    def index() {
        render pessoaService.findAllOrderByName()
    }
}