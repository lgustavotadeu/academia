package academia


class LoginController {

    static allowedMethods = [
            login : "POST"
    ]

    LoginService loginService

    def login() {
        render loginService.login(request, response)
    }

}
