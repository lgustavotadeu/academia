package academia

import java.text.SimpleDateFormat

@Singleton
class TokenUtil {

    String formatToken (Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format)
        return simpleDateFormat.format(date)
    }
}
