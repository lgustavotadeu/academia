package academia

import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject

@Singleton
class NotFoundResponseUtil {

    JSONObject createNotFoundResponse(String message) {
        JSONArray jsonArray = new JSONArray()
        jsonArray.add(message)

        return new JSONObject(
                'errors': jsonArray
        )
    }
}
