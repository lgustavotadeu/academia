package academia

import org.grails.web.json.JSONObject

@Singleton
class PessoaUtil {

    JSONObject getPessoaJSONObject(Pessoa pessoa) {
        JSONObject pessoaJSONObject = new JSONObject(
                [
                        'id': pessoa.id,
                        'nome': pessoa.nome,
                        'email': pessoa.email,
                        'token': pessoa.token

                ]

        )
        return pessoaJSONObject
    }
}
