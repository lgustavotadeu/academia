package academia

import java.text.SimpleDateFormat

@Singleton
class DateUtil {

    String formatDate (Date date, String format) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(format)
        return simpleDateFormat.format(date)
    }

    String formatDate (Date date) {
        return date.getDateString()
    }
}
