package academia

import org.grails.web.json.JSONObject

@Singleton
class ContratoUtil {

    JSONObject getContratoJSONObject(Contrato contrato) {
        JSONObject contratoJSONObject = new JSONObject(
                [
                        'id': contrato.id,
                        'dataCriacao': DateUtil.instance.formatDate(contrato.dataCriacao),
                        'dataFim': DateUtil.instance.formatDate(contrato.dataFim),
                        'pessoa': contrato.pessoa
                ]
        )
        return contratoJSONObject
    }
}
