package academia

class Permissao implements Serializable {

    String descricao

    static hasMany = [pessoa:Pessoa]

    static constraints = {
        descricao nullable: false
    }

    static mapping = {
        id generator: 'increment'

        version false
    }
}
