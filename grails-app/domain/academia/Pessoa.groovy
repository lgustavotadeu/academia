package academia

class Pessoa implements Serializable {

    String nome
    String cpf
    String email
    String senha
    String sexo
    Date dataNasc
    Boolean ativo
    String token
    Date tokenExpirationDate
    String urlFoto

    Permissao permissao

    static hasMany = [contrato:Contrato]

    static constraints = {
        email email: true, nullable: false, unique: true
        sexo inList: ['M','F']
        dataNasc max: new Date()
        senha size: 6..10
        token nullable: true, maxSize: 32
        tokenExpirationDate nullable: true, sqlType: 'datetime'

    }

    static mapping = {
        version false
    }
}