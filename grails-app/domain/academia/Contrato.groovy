package academia

class Contrato {

    Double valor
    Date dataCriacao
    Date dataFim
    int status

    Pessoa pessoa

    static constraints = {
        valor nullable: false
        dataCriacao min: new Date()
        dataFim min: new Date()
        status inList: [1,2],nullable: false
    }

    static mapping = {
        version false
    }
}
