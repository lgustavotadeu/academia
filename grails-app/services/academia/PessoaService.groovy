package academia

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonSlurper
import org.apache.catalina.connector.RequestFacade
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus


import javax.servlet.http.HttpServletResponse

@Transactional
class PessoaService {

    MessageSource messageSource

    JSONObject show(GrailsParameterMap params, HttpServletResponse response){
        JSONObject responseJSONObject = new JSONObject()

        try{
            Pessoa pessoa = Pessoa.read(DataUtil.instance.getLong(params.id))

            if (!pessoa) {
                throw new IllegalArgumentServiceException('academia.Login.not.found', HttpStatus.NOT_FOUND.value())
            }

            responseJSONObject = PessoaUtil.instance.getPessoaJSONObject(pessoa)
            response.status = HttpStatus.OK.value()

        } catch (IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }

        return  responseJSONObject
    }


    JSONObject save (RequestFacade request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        try {
            JsonSlurper jsonSlurper = new JsonSlurper()
            Pessoa pessoa = new Pessoa(jsonSlurper.parseText(jsonObject.toString()))

            pessoa.ativo = true
            pessoa.tokenExpirationDate = new Date().clearTime()
            pessoa.token = TokenUtil.instance.formatToken(new Date(), 'ddMMyyyyHHmmss')

            if (!pessoa.validate()) {
                throw new IllegalArgumentServiceException(pessoa.errors)
            }

            if (validateCpf(pessoa.cpf)) {
                pessoa.save(flush: true, failOnError: true)
            } else {
                throw new IllegalArgumentServiceException('academia.User.cpf.nullable', HttpStatus.NOT_FOUND.value())
            }

            responseJSONObject = PessoaUtil.instance.getPessoaJSONObject(pessoa)
            response.status = HttpStatus.CREATED.value()


        } catch (IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }


        return responseJSONObject
    }


    JSONObject delete (GrailsParameterMap params, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()

        Pessoa pessoa = Pessoa.findById(DataUtil.instance.getLong(params.id))

        if (pessoa) {
            pessoa.delete(flush: true, failOnError: true)
            response.status = HttpStatus.NO_CONTENT.value()
        } else {
            responseJSONObject = NotFoundResponseUtil.instance.createNotFoundResponse(messageSource.getMessage('academia.Pessoa.not.found', null, null))
            response.status = HttpStatus.NOT_FOUND.value()
        }

        return responseJSONObject
    }


    JSONObject update (GrailsParameterMap params, RequestFacade request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        Pessoa pessoa = Pessoa.findById(DataUtil.instance.getLong(params.id))

        if (pessoa) {
            try {
                JsonSlurper jsonSlurper = new JsonSlurper()
                pessoa.properties = jsonSlurper.parseText(jsonObject.toString())

                if (!pessoa.validate()) {
                    throw new IllegalArgumentServiceException(pessoa.errors)
                }

                pessoa.save(flush: true, failOnError: true)

                responseJSONObject = PessoaUtil.instance.getPessoaJSONObject(pessoa)
                response.setStatus(HttpStatus.OK.value())

            } catch (IllegalArgumentServiceException e) {
                responseJSONObject.put('errors', e.getMessages(messageSource))
                response.status  HttpStatus.NOT_FOUND.value()
            }
        }

        return responseJSONObject
    }


    JSONObject findAllOrderByName() {
        JSONObject responseJSONObject = new JSONObject()
        JSONArray pessoasJSONArray = new JSONArray()

        List<Pessoa> pessoas = Pessoa.findAll().sort( { it.nome.toLowerCase() } )

        pessoas.each { Pessoa pessoa ->
            JSONObject pessoaJSONObject = new JSONObject()
            pessoaJSONObject = PessoaUtil.instance.getPessoaJSONObject(pessoa)

            pessoasJSONArray.put(pessoaJSONObject)
        }

        return  responseJSONObject.put('pessoas', pessoasJSONArray)

    }


    def validateCpf(String cpf){

        try{
            boolean validado = true
            int d1, d2
            int dg1, dg2, resto
            int dgcpf

            d1 = d2 = 0
            dg1 = dg2 = resto = 0

            for (int i = 1; i < cpf.length() -1; i++) {
                dgcpf = cpf.substring(i -1, i) as Integer
                d1 = d1 + ( 11 - i ) * dgcpf
                d2 = d2 + ( 12 - i ) * dgcpf
            }

            resto = (d1 % 11)
            dg1 = (resto < 2) ? 0 : 11 - resto

            d2 += 2 * dg1
            resto = (d2 % 11)
            dg2 = (resto < 2) ? 0 : 11 - resto

            def dgverif = cpf.substring(cpf.length()-2)
            def dgresult = (dg1 as String)  + (dg2 as String)

            if (dgverif == dgresult) {
                return validado
            } else {
                validado = false
            }

            return validado
        }catch (Exception e){
            return false
        }
    }





}
