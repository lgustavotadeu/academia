package academia

import grails.gorm.transactions.Transactional
import grails.web.servlet.mvc.GrailsParameterMap
import groovy.json.JsonSlurper
import jdk.nashorn.internal.ir.IfNode
import org.apache.catalina.connector.RequestFacade
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletResponse

@Transactional
class LoginService {

    MessageSource messageSource


    JSONObject login(RequestFacade request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        String email = jsonObject.email as String
        String senha = jsonObject.senha as String

        try {
            emailAndPasswordNotNull(email, senha)

            Pessoa pessoa = Pessoa.findByEmail(email)

            if (!pessoa) {
                throw new IllegalArgumentServiceException('academia.Login.not.found', HttpStatus.NOT_FOUND.value())
            }

            if (pessoa.ativo == true) {

                if (pessoa.email == email && pessoa.senha == senha) {

                    updateToken(pessoa)

                    responseJSONObject = PessoaUtil.instance.getPessoaJSONObject(pessoa)
                    response.status = HttpStatus.OK.value()

                } else {
                    throw new IllegalArgumentServiceException('academia.Login.loginorpassword.invalid', HttpStatus.NOT_FOUND.value())
                }
            } else {
                throw new IllegalArgumentServiceException('academia.Login.loginorpassword.block', HttpStatus.UNAUTHORIZED.value())
            }


        } catch (IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())

        }

        return responseJSONObject
    }


    private void emailAndPasswordNotNull(String email, String senha) throws IllegalArgumentServiceException {
        if (!email) {
            throw new IllegalArgumentServiceException('academia.Login.email.nullable', HttpStatus.NOT_FOUND.value())
        }

        if (!senha) {
            throw new IllegalArgumentServiceException('academia.Login.password.nullable', HttpStatus.NOT_FOUND.value())
        }
    }

    private void updateToken (Pessoa pessoa) {

        pessoa.token = TokenUtil.instance.formatToken(new Date(), 'ddMMyyyyHHmmss')
        pessoa.save(flush: true, failOnError: true)
    }


}
