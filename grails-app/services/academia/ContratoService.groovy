package academia

import grails.gorm.transactions.Transactional
import groovy.json.JsonSlurper
import org.apache.catalina.connector.RequestFacade
import org.grails.web.json.JSONArray
import org.grails.web.json.JSONObject
import org.springframework.context.MessageSource
import org.springframework.http.HttpStatus

import javax.servlet.http.HttpServletResponse

@Transactional
class ContratoService {

    MessageSource messageSource

    JSONObject save (RequestFacade request, HttpServletResponse response) {
        JSONObject responseJSONObject = new JSONObject()
        JSONObject jsonObject = request.JSON

        try {
            JsonSlurper jsonSlurper = new JsonSlurper()
            Contrato contrato = new Contrato(jsonSlurper.parseText(jsonObject.toString()))

            contrato.status = 1
            contrato.dataCriacao = new Date()

            if (!contrato.validate()) {
                throw new IllegalArgumentServiceException(contrato.errors)
            }

            contrato.save(flush: true, failOnError: true)

            responseJSONObject = ContratoUtil.instance.getContratoJSONObject(contrato)
            response.status = HttpStatus.CREATED.value()


        } catch (IllegalArgumentServiceException e) {
            responseJSONObject.put('errors', e.getMessages(messageSource))
            response.setStatus(e.getHttpCode())
        }


        return responseJSONObject
    }


    JSONObject oneDayToEndContract () {
        JSONObject responseJSONObject = new JSONObject()
        JSONArray contratosJSONArray = new JSONArray()


        List<Contrato> contratos = Contrato.findAllByDataCriacao(new Date())

        String contractEndDay = DateUtil.instance.formatDate(new Date(), 'EEE')

        contratos.each { Contrato contrato ->
            JSONObject contratoJSONObject = new JSONObject()

            if (endDayContract == 'Sab') {
                endDayContract = contrato.dataFim+2
            }
            if (endDayContract == 'Doming') {
                endDayContract = contrato.dataFim+1
            }

            contratoJSONObject = ContratoUtil.instance.getContratoJSONObject(contrato)

            contratosJSONArray.put(contratoJSONObject)
        }

        return  responseJSONObject.put('contratos', contratosJSONArray)

    }
}
