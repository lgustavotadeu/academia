package academia

import grails.gorm.transactions.Transactional
import org.springframework.context.MessageSource


@Transactional
class AuthenticationService {

    MessageSource messageSource

    boolean authorized(Pessoa pessoa, String token) {
        boolean authorized = false

        if (!token || !pessoa.token || pessoa.token != token || !pessoa.tokenExpirationDate || isTokenExpired(pessoa.tokenExpirationDate)) {
            throw new IllegalArgumentServiceException(messageSource, 'academia.User.token.nullable')
            return authorized
        }

        if (pessoa.permissao.id == 1) {
            authorized = true
        }

        return authorized
    }

    private boolean isTokenExpired(Date tokenExpirationDate) {

        /** Padroniza o formato das datas. */
        String currentDateString = new Date().format("dd/MM/yyyy")
        String tokenExpirationDateString = DateUtil.instance.formatDate(tokenExpirationDate, 'dd/MM/yyyy')


        if (currentDateString > tokenExpirationDateString) {
            return true
        } else {
            return false
        }
    }



}
